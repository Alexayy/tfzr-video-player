package controller;

import com.google.gson.Gson;
import model.Project;
import service.ProjectServices;
import utils.JsonTransformer;

import java.util.ArrayList;
import java.util.UUID;

import static spark.Spark.*;

public class ProjectRouteController extends Controller {

    private static final int HTTP_BAD_REQUEST = 400;
    public ProjectServices projectServices;

    public ProjectRouteController() {
        projectServices = new ProjectServices();
    }

    public void api() {
        get("/projects", ((request, response) -> {
            response.type("application/json");
            return projectServices.findProjectByUUID(uuid.getProjectUUID());

        }), new JsonTransformer());
    }

    public void getCalled(UUID uuid) {
        get("/projects", ((request, response) -> projectServices.findProjectByUUID(uuid)), new JsonTransformer());
    }

    public void getCallAll() {
        get("/allprojects", (request, response) -> projectServices.displayAllProjects(), new JsonTransformer());
    }

    public void getCreated(UUID uuid, UUID userUUID, UUID skillUUID, String name) {
        post("/create", (request, response) -> {
            projectServices.addProject(new Project(uuid, userUUID, skillUUID, name));
            return uuid;
        }, new JsonTransformer());
    }

    public void createProjects(ArrayList<Project> projectLists) {
        post("/insert", (request, response) -> projectServices.addProjects(projectLists), new JsonTransformer());
    }

    public void updateProject(UUID uuid, Project project) {
        post("/update", ((request, response) -> {
            projectServices.updateProject(uuid, project);
            return uuid;
        }), new JsonTransformer());
    }

    public void deleteProject (UUID uuid) {
        delete("/uuid", (request, response) -> {
            projectServices.deleteProject(uuid);
            return uuid;
        });
    }
}
