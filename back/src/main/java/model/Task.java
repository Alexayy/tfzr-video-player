package model;

import java.sql.Time;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class Task {
    private UUID taskUUID;
    private UUID userUUID;
    private UUID skillUUID;
    private String name;
    private int difficulty;
    private Date estimatedTime;
    private Time userFinishTime;
    private Collection<Skill> skillsRequired;
    private Collection<User> usersFinished;

    public Task() {

    }

    public Task(UUID taskUUID, UUID userUUID, UUID skillUUID,
                String name, int difficulty, Date estimatedTime,
                Time userFinishTime, Collection<Skill> skillsRequired,
                Collection<User> usersFinished) {
        this.taskUUID = taskUUID;
        this.userUUID = userUUID;
        this.skillUUID = skillUUID;
        this.name = name;
        this.difficulty = difficulty;
        this.estimatedTime = estimatedTime;
        this.userFinishTime = userFinishTime;
        this.skillsRequired = skillsRequired;
        this.usersFinished = usersFinished;
    }
}
