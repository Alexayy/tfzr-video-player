package model;
import java.util.UUID;

public class Role {
    private UUID roleUuid;
    private UUID userUuid;
    private String roleName;

    public Role(UUID roleUuid, UUID userUuid, String roleName) {
        this.roleUuid = roleUuid;
        this.userUuid = userUuid;
        this.roleName = roleName;
    }

    public Role() { }

    public UUID getRoleUuid() {
        return roleUuid;
    }

    public void setRoleUuid(UUID roleUuid) {
        this.roleUuid = roleUuid;
    }

    public UUID getUserUuid() {
        return userUuid;
    }

    public void setUserUuid(UUID userUuid) {
        this.userUuid = userUuid;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
