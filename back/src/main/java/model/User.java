package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.UUID;

/**
 * Make Constructors as needed per call for instances that we might need.
 */
public class User {
    private UUID userUuid;
    private UUID mentor;
    private UUID skillUuid;
    private UUID taskUuid;
    private UUID projectUuid;
    private String name;
    private String surname;
    private Date dateOfBirth;
    private Date dateOfJoining;
    private UUID role;
    private String email;
    private String password;
    private String image;
    private String phoneNumber;

    public static final String USERS = "users";
    public static final String COLUMN_USERUUID = "useruuid";
    public static final String COLUMN_SKILLUUID = "skilluuid";
    public static final String COLUMN_TASKUUID = "taskuuid";
    public static final String COLUMN_PROJECTUUID = "projectuuid";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SURNAME = "surname";
    public static final String COLUMN_DATEOFBIRTH = "dateofbirth";
    public static final String COLUMN_DATEOFJOINING = "dateofjoining";
    public static final String COLUMN_ROLE = "role";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_PICTURE = "picture";
    public static final String COLUMN_PHONENUMBER = "phonenumber";

    public User() {

    }

    /**
     * @param userUuid
     * @param mentor
     * @param skillUuid
     * @param taskUuid
     * @param projectUuid
     * @param name
     * @param surname
     * @param dateOfBirth
     * @param dateOfJoining
     * @param role
     * @param email
     * @param password
     * @param image
     * @param phoneNumber
     */
    public User(UUID userUuid, UUID mentor, UUID skillUuid, UUID taskUuid, UUID projectUuid,
                String name, String surname, Date dateOfBirth, Date dateOfJoining,
                UUID role, String email, String password, String image, String phoneNumber) {
        this.userUuid = userUuid;
        this.mentor = mentor;
        this.skillUuid = skillUuid;
        this.taskUuid = taskUuid;
        this.projectUuid = projectUuid;
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = new Date();
        this.dateOfJoining = new Date();
        this.role = role;
        this.email = email;
        this.password = password;
        this.image = image;
        this.phoneNumber = phoneNumber;
    }

    /**
     * Hardcoded adding of Users
     *
     * @param user
     */
    public void addUser(User user) {
        // TODO Rrefactor this from hard coding to JAVA SPARK PATHING !!!
        UUID userGeneratedUUID = UUID.randomUUID();
        UUID skillGeneratedUUID = UUID.randomUUID();
        UUID taskGeneratedUUID = UUID.randomUUID();
        UUID projectGeneratedUUID = UUID.randomUUID();
        UUID mentorGeneratedUUID = UUID.randomUUID();
        UUID roleGeneratedUUID = UUID.randomUUID();
        try {
            Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "root");
            Statement st = connection.createStatement();
            insertUser(st, userGeneratedUUID, mentorGeneratedUUID, skillGeneratedUUID, taskGeneratedUUID, projectGeneratedUUID,
                    "Caka", "Aleksic", null, null,
                    roleGeneratedUUID, "aleksa.cakic@gmail.com", "jebemtijdbc", null, "069737988");

            System.out.println("Connected to utils.DB!");
            System.out.println("Entered the Values! ");
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public UUID generateUUID() {
        UUID uuid = UUID.randomUUID();
        userUuid = uuid;
        System.out.println(userUuid);
        return userUuid;
    }

    /**
     * Hardcoded query method
     *
     * @param statement
     * @param userUuid
     * @param mentor
     * @param skillUuid
     * @param taskUuid
     * @param projectUuid
     * @param name
     * @param surname
     * @param dateOfBirth
     * @param dateOfJoining
     * @param role
     * @param email
     * @param password
     * @param image
     * @param phoneNumber
     * @throws SQLException
     */
    private static void insertUser(Statement statement, UUID userUuid, UUID mentor, UUID skillUuid, UUID taskUuid, UUID projectUuid,
                                   String name, String surname, Date dateOfBirth, Date dateOfJoining,
                                   UUID role, String email, String password, String image, String phoneNumber) throws SQLException {

        // Query instruction statement used to introduce INSERTION into utils.DB
        statement.execute("INSERT INTO " + USERS + "( " + COLUMN_USERUUID + ", " + COLUMN_SKILLUUID + ", " + COLUMN_TASKUUID + ", " + COLUMN_PROJECTUUID
                + ", " + COLUMN_NAME + ", " + COLUMN_SURNAME + ", " + COLUMN_DATEOFBIRTH + ", " + COLUMN_DATEOFJOINING + ", " + COLUMN_ROLE + ", "
                + COLUMN_EMAIL + ", " + COLUMN_PASSWORD + ", " + COLUMN_PICTURE + ", " + COLUMN_PHONENUMBER + ")"
                // Query instruction to take values
                + " VALUES('" + userUuid + "', '" + skillUuid + "', '" + taskUuid + "', '" + projectUuid + "', '" + name + "', '" + surname + "', '"
                + dateOfBirth + "', '" + dateOfJoining + "', '" + role + "', '" + email + "', '" + password + "', '" + image + "', '" + phoneNumber + "')");
    }

    /**
     * Beneath are getters and setters
     */

    public void setUserUuid(UUID userUuid) {
        this.userUuid = userUuid;
    }


    public UUID getUserUuid() {
        return userUuid;
    }

    public UUID getMentor() {
        return mentor;
    }

    public void setMentor(UUID mentor) {
        this.mentor = mentor;
    }

    public UUID getSkillUuid() {
        return skillUuid;
    }

    public void setSkillUuid(UUID skillUuid) {
        this.skillUuid = skillUuid;
    }

    public UUID getTaskUuid() {
        return taskUuid;
    }

    public void setTaskUuid(UUID taskUuid) {
        this.taskUuid = taskUuid;
    }

    public UUID getProjectUuid() {
        return projectUuid;
    }

    public void setProjectUuid(UUID projectUuid) {
        this.projectUuid = projectUuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateOfJoining() {
        return dateOfJoining;
    }

    public void setDateOfJoining(Date dateOfJoining) {
        this.dateOfJoining = dateOfJoining;
    }

    public UUID getRole() {
        return role;
    }

    public void setRole(UUID role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
