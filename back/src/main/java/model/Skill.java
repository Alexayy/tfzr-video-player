package model;

import org.graalvm.compiler.bytecode.BytecodeStream;

import java.util.Collection;
import java.util.UUID;

public class Skill {
    private UUID skillUUID;
    private UUID userUUID;
    private UUID taskUUID;
    private UUID projectUUID;
    private String name;
    private BytecodeStream skillLogo;

    public Skill(UUID skillUUID, UUID userUUID, UUID taskUUID,
                 UUID projectUUID, String name, BytecodeStream skillLogo) {
        this.skillUUID = skillUUID;
        this.userUUID = userUUID;
        this.taskUUID = taskUUID;
        this.projectUUID = projectUUID;
        this.name = name;
        this.skillLogo = skillLogo;
    }

    public Skill() {

    }
}
