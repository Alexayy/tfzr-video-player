package model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.UUID;

public class Project {
    private UUID projectUUID;
    private UUID userUUID;
    private UUID skillUUID;
    private String name;
    private LocalDate estimatedTime;
    private LocalDate deliveryTime;
    private Date dueDate;
    private Currency salary;
    private Collection<Skill> listOfSkills;
    private Collection<User> listOfUsers;

    public Project(UUID projectUUID, UUID userUUID, UUID skillUUID,
                   String name, LocalDate estimatedTime, LocalDate deliveryTime,
                   Date dueDate, Currency salary,
                   Collection<Skill> listOfSkills, Collection<User> listOfUsers) {
        this.projectUUID = projectUUID;
        this.userUUID = userUUID;
        this.skillUUID = skillUUID;
        this.name = name;
        this.estimatedTime = estimatedTime;
        this.deliveryTime = deliveryTime;
        this.dueDate = dueDate;
        this.salary = salary;
        this.listOfSkills = listOfSkills;
        this.listOfUsers = listOfUsers;
    }

    public Project(UUID projectUUID, UUID userUUID, UUID skillUUID,
                   String name, LocalDate estimatedTime, LocalDate deliveryTime,
                   Date dueDate, Currency salary) {
        this.projectUUID = projectUUID;
        this.userUUID = userUUID;
        this.skillUUID = skillUUID;
        this.name = name;
        this.estimatedTime = estimatedTime;
        this.deliveryTime = deliveryTime;
        this.dueDate = dueDate;
        this.salary = salary;
    }

    public Project(UUID projectUUID, String name) {
        this.projectUUID = projectUUID;
        this.name = name;
    }

    public Project(UUID projectUUID, UUID userUUID, UUID skillUUID, String name) {
        this.projectUUID = projectUUID;
        this.userUUID = userUUID;
        this.skillUUID = skillUUID;
        this.name = name;
    }

    public Project(UUID projectUUID, String nameBoi, String place) {
        this.projectUUID = projectUUID;
        this.userUUID = getUserUUID();
        this.skillUUID = getSkillUUID();
        this.name = getName();
        this.name = nameBoi;
        setName(name);
    }

    public Project(Project project) {
        this.projectUUID = projectUUID;
        this.userUUID = userUUID;
        this.skillUUID = skillUUID;
        this.name = name;
        this.estimatedTime = estimatedTime;
        this.deliveryTime = deliveryTime;
        this.dueDate = dueDate;
        this.salary = salary;
    }

    public Project() {
    }

    public UUID getProjectUUID() {
        return projectUUID;
    }

    public void setProjectUUID(UUID projectUUID) {
        this.projectUUID = projectUUID;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public UUID getSkillUUID() {
        return skillUUID;
    }

    public void setSkillUUID(UUID skillUUID) {
        this.skillUUID = skillUUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(LocalDate estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public LocalDate getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(LocalDate deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Currency getSalary() {
        return salary;
    }

    public void setSalary(Currency salary) {
        this.salary = salary;
    }

    public Collection<Skill> getListOfSkills() {
        return listOfSkills;
    }

    public void setListOfSkills(Collection<Skill> listOfSkills) {
        this.listOfSkills = listOfSkills;
    }

    public Collection<User> getListOfUsers() {
        return listOfUsers;
    }

    public void setListOfUsers(Collection<User> listOfUsers) {
        this.listOfUsers = listOfUsers;
    }
}
