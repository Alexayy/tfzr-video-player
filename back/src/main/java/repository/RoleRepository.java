package repository;

import model.Role;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class RoleRepository implements BaseRepository<Role> {
    @Override
    public Connection connection() throws SQLException {
        return null;
    }

    @Override
    public void create(Role o) {

    }

    @Override
    public Role update() {
        return null;
    }

    @Override
    public void delete(UUID deleteByUuid) {

    }

    @Override
    public Role getById(UUID uuid) {
        return null;

    }
}