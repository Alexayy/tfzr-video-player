package repository;

import model.Project;
import org.postgresql.util.PSQLException;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.UUID;

public class ProjectRepository implements BaseRepository<Project> {

    /**
     * The following connect() method establishes a connection to the dvdrental sample database and returns a Connection object.
     *
     * @return Connection
     * @throws SQLException If there is a mistake
     */
    @Override
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(BaseRepository.URL + BaseRepository.HOST + ":" + BaseRepository.PORT
                + "/" + BaseRepository.DBNAME + "?sslmode=disable", BaseRepository.USERNAME, BaseRepository.PASSWORD);
    }

    /**
     * TODO Predlog da ovo bude AddMoreThanOne sinature metoda. *imenovati po konvenciji*
     *
     * @param list
     * @return
     */
    public List insertProjects(List<Project> list) {
        String sql = "INSERT INTO public.projects(projectuuid, useruuid, skilluuid, \"name\", estimatedtime, deliverytime, duedate, salary) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            int count = 0;

            for (Project project : list) {
                setProjectFromPrepareStatemen(preparedStatement, project);

                preparedStatement.addBatch();
                count++;

                if (count % 100 == 0 || count == list.size())
                    preparedStatement.executeBatch();

            }
        } catch (SQLException e) {
            System.out.println("Problem With insertProjects method in ProjectRepository class");
            System.out.println(e.getMessage());
        }

        return list;
    }

    private void setProjectFromPrepareStatemen(PreparedStatement preparedStatement, Project project) throws SQLException {
        preparedStatement.setObject(1, project.getProjectUUID(), Types.OTHER);
        preparedStatement.setObject(2, project.getUserUUID(), Types.OTHER);
        preparedStatement.setObject(3, project.getSkillUUID(), Types.OTHER);
        preparedStatement.setString(4, project.getName());
        preparedStatement.setObject(5, project.getEstimatedTime(), Types.OTHER);
        preparedStatement.setObject(6, project.getDeliveryTime(), Types.OTHER);
        preparedStatement.setObject(7, project.getDueDate(), Types.OTHER);
        preparedStatement.setObject(8, project.getSalary(), Types.OTHER);
    }

    /**
     * TODO predlog da ovo bude signature metode
     *
     * @param uuid
     * @param project
     */
    public void updateA(UUID uuid, Project project) {
        String sql = "UPDATE public.projects SET projectuuid = ?, useruuid = ?, skilluuid = ?, \"name\" = ?, estimatedtime = ?, deliverytime = ?, duedate = ?, salary = ? WHERE projectuuid = ?";

        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            int tableRow = 1;

            preparedStatement.setObject(tableRow++, uuid, Types.OTHER);
            preparedStatement.setObject(tableRow++, project.getUserUUID(), Types.OTHER);
            preparedStatement.setObject(tableRow++, project.getSkillUUID(), Types.OTHER);
            preparedStatement.setString(tableRow++, project.getName());
            preparedStatement.setObject(tableRow++, project.getEstimatedTime(), Types.OTHER);
            preparedStatement.setObject(tableRow++, project.getDeliveryTime(), Types.OTHER);
            preparedStatement.setObject(tableRow++, project.getDueDate(), Types.OTHER);
            preparedStatement.setObject(tableRow++, project.getSalary(), Types.OTHER);
            preparedStatement.setObject(tableRow++, uuid, Types.OTHER);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Something Went Wrong in Project Update Method! In Project Repository!");
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void delete(UUID deleteByUuid) {
        String sql = "DELETE FROM public.projects WHERE projectuuid = ?";
        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setObject(1, deleteByUuid, Types.OTHER);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("ProjectRepository class mistake, in delete method");
            System.out.println(e.getMessage());
        }
    }

    public void create(Project o) {
        String sql = "INSERT INTO public.projects(projectuuid, useruuid, skilluuid, \"name\", estimatedtime, deliverytime, duedate, salary) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            setProjectFromPrepareStatemen(preparedStatement, o);

            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.out.println("-----------------------------------------------------------");
            System.out.println("Create operation in Project Repository is misbehaving!");
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace());
            System.out.println("-----------------------------------------------------------");
        }
    }

    /**
     * The ResultSet is from this Overridden method as a methods local variable.
     */
    @Override
    public Project getById(UUID uuid) {
        String sql = "SELECT * FROM public.projects";
        Project project = new Project();
        try (Connection connection = connection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)) {
            project = displayQuery(resultSet);
            return displayQuery(resultSet);
        } catch (PSQLException e) {
            System.out.println("Postgres fucked up");
            e.getMessage();
        } catch (SQLException e) {
            System.out.println("General SQL fuckup");
            e.getMessage();
        }

        return project;
    }

    @Override
    public Project update() {
        return null;
    }

    /**
     * TODO predlog da ovo bude signature metode
     *
     * @return
     */
    public ArrayList<Project> getAll() {
        String sql = "SELECT * FROM public.projects";
        ArrayList<Project> list = new ArrayList();
        ResultSet resultSet = null;
        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                Project project = new Project();
                setProjectFromResultSet(resultSet, project);
                list.add(project);
            }

            preparedStatement.executeQuery();

        } catch (PSQLException e) {
            System.out.println("Postgres fucked up");
            System.out.println(e.getMessage());
        } catch (SQLException e) {
            System.out.println("General SQL fuckup");
            System.out.println(e.getMessage());
        }

        return list;
    }

    /**
     * TODO Predlog da ovo bude signature metode
     *
     * @param resultSet
     * @return
     * @throws SQLException
     */
    public Project displayQuery(ResultSet resultSet) throws SQLException {
        Project project = new Project();
        while (resultSet.next()) {
            setProjectFromResultSet(resultSet, project);
        }

        return project;
    }

    private void setProjectFromResultSet(ResultSet resultSet, Project project) throws SQLException {
        project.setProjectUUID(UUID.fromString(resultSet.getString("projectuuid")));
        project.setUserUUID(UUID.fromString(resultSet.getString("useruuid")));
        project.setSkillUUID(UUID.fromString(resultSet.getString("skilluuid")));
        project.setName(resultSet.getString("name"));
        project.setEstimatedTime((LocalDate) resultSet.getObject("estimatedtime"));
        project.setDeliveryTime((LocalDate) resultSet.getObject("deliverytime"));
        project.setDueDate(resultSet.getDate("duedate"));
        project.setSalary((Currency) resultSet.getObject("salary"));
    }

    public Project findByUUID(UUID uuid) {
        String sql = "SELECT projectuuid, useruuid, skilluuid, name FROM public.projects WHERE projectuuid = ?";

        Project project = new Project();
        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setObject(1, uuid, Types.OTHER);
            ResultSet resultSet = preparedStatement.executeQuery();
            project = displayQuery(resultSet);
            displayQuery(resultSet);
            return displayQuery(resultSet);

        } catch (SQLException e) {
            System.out.println("SQL is a bit fucky whacky, and here's why: ");
            System.out.println(e.getMessage());
        }

        return project;
    }

    public Project findByName(String name) {
        String sql = "SELECT * FROM public.projects WHERE name = ?";

        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setObject(1, name, Types.VARCHAR);
            ResultSet resultSet = preparedStatement.executeQuery();
            displayQuery(resultSet);
        } catch (SQLException e) {
            System.out.println("Method find by name is misbehaving!");
            System.out.println(e.getMessage());
        }

        return null;
    }

    public Project findBySkill(UUID skillUuid) {
        String sql = "SELECT * FROM public.projects WHERE skilluuid=?";

        try (Connection connection = connection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setObject(1, skillUuid, Types.OTHER);
            ResultSet resultSet = preparedStatement.executeQuery();
            displayQuery(resultSet);
        } catch (SQLException e) {
            System.out.println("FindBySKill method is misbehaving!");
            System.out.println(e.getMessage());
        }
        return null;
    }
}