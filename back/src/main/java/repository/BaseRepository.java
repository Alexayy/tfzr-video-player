/**
 * @author Adam Rumun
 */

package repository;

import model.Project;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public interface BaseRepository<T> {
    String HOST = "127.0.0.1";
    String PORT = "5432";
    String USERNAME = "postgres";
    String PASSWORD = "root";
    String URL = "jdbc:postgresql://";
    String DBNAME = "postgres";

    Connection connection() throws SQLException;

    void create(T o); // Done

    T update(); // Done

    void delete(UUID deleteByUuid); // Done

    T getById(UUID uuid);
}