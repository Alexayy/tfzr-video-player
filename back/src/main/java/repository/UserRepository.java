package repository;

import model.User;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.sql.*;
import java.util.UUID;

public class UserRepository implements BaseRepository<User> {
//

    @Override
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(BaseRepository.URL + BaseRepository.HOST + ":" + BaseRepository.PORT
                + "/" + BaseRepository.DBNAME + "?sslmode=disable", BaseRepository.USERNAME, BaseRepository.PASSWORD);
    }

    @Override
    public void create(User o) {

    }

    @Override
    public User update() {
        return null;
    }

    @Override
    public void delete(UUID deleteByUuid) {

    }

    public User displayQuery(ResultSet resultSet) throws SQLException {
        return null;

    }

    @Override
    public User getById(UUID uuid) {
        String query = "SELECT * FROM public.users WHERE useruuid = 'bd38f268-57d7-48cf-8c33-4c9599fb844b'";

        try (Connection con = connection()) {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(query);

            User user = new User();
            user.setUserUuid(UUID.fromString(rs.getString("useruuid")));
            return user;
//            return rs.getString("useruuid");
//            return rs.getObject(0, User.class);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
