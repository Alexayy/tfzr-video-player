package service;

import model.User;
import repository.UserRepository;

import java.util.List;
import java.util.UUID;

public class UserService {

    public UserRepository userRepository;

    public UserService() {
        userRepository = new UserRepository();
    }


    public User getUserById(UUID uuid) {
        return userRepository.getById(uuid);
    }
}
