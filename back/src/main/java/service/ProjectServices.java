package service;

import model.Project;
import repository.ProjectRepository;

import java.util.List;
import java.util.UUID;

public class ProjectServices {
    //ProjectRepository projectRepository;
    private static final int HTTP_BAD_REQUEST = 400;
    public ProjectRepository projectServices;

    public ProjectServices() {
        projectServices = new ProjectRepository();
    }

    public void addProject(Project project) {
        projectServices.create(project);
    }

    public List<Project> addProjects(List<Project> list) {
        return projectServices.insertProjects(list);
    }

    public UUID updateProject(UUID uuid, Project project) {
        projectServices.updateA(uuid, project);
        return uuid;
    }

    public UUID deleteProject(UUID uuid) {
        projectServices.delete(uuid);
        return uuid;
    }

    public Project findProjectByUUID(UUID uuid) {
        return projectServices.getById(uuid);
    }

    public List<Project> displayAllProjects() {
        return projectServices.getAll();
    }

}
