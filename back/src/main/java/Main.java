import controller.ProjectRouteController;
import utils.CorsFilter;

import java.util.UUID;

import static spark.Spark.init;

public class Main {
    /**
     * @param args
     */

    public static void main(String[] args) {
        // Java Spark model.project is always on port:
        // http://localhost:4567/hello
        CorsFilter.apply();
        ProjectRouteController projectRouteController = new ProjectRouteController();
        UUID aleksa = UUID.fromString("3c2c1610-191f-4110-ba89-b444e0d1f1c9");
//        projectRouteController.updateProject(aleksa, new Project(aleksa, "What?"));
//        projectRouteController.getCallAll();
//        ArrayList<Project> list = new ArrayList<>();
//        list.add(new Project(UUID.randomUUID(), UUID.fromString("bd38f268-57d7-48cf-8c33-4c9599fb844b"), UUID.fromString("371b8347-ec1f-4f20-8e25-31a7d3d1b0e0"), "Bocko Pomaze vise od Adama"));
//        projectRouteController.createProjects(list);
//        projectRouteController.getCreated(UUID.randomUUID(), UUID.fromString("bd38f268-57d7-48cf-8c33-4c9599fb844b"), UUID.fromString("371b8347-ec1f-4f20-8e25-31a7d3d1b0e0"), "Ne znam da li radi");

//        projectRouteController.updateProject(aleksa, new Project(aleksa, "Aleksa C"));
        projectRouteController.getCallAll();
        projectRouteController.api();
        init();
    }
}
