-- Drop table

-- DROP TABLE public.skills;

CREATE TABLE IF NOT EXISTS public.skills
(
    skilluuid   uuid    NOT NULL,
    "name"      varchar NULL,
    useruuid    uuid    NULL,
    taskuuid    uuid    NULL,
    projectuuid uuid    NULL,
    skilllogo   bytea   NULL,
    CONSTRAINT skills_pk PRIMARY KEY (skilluuid)
);

ALTER TABLE public.skills
    ADD CONSTRAINT skills_fk FOREIGN KEY (useruuid) REFERENCES users (useruuid);
ALTER TABLE public.skills
    ADD CONSTRAINT skills_fk_1 FOREIGN KEY (projectuuid) REFERENCES projects (projectuuid);
ALTER TABLE public.skills
    ADD CONSTRAINT skills_fk_2 FOREIGN KEY (taskuuid) REFERENCES tasks (taskuuid);


