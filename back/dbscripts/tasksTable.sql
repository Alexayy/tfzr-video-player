-- Drop table

-- DROP TABLE public.tasks;

CREATE TABLE IF NOT EXISTS public.tasks
(
    taskuuid       uuid      NOT NULL,
    useruuid       uuid      NULL,
    skilluuid      uuid      NULL,
    "name"         varchar   NOT NULL,
    difficulty     int4      NULL,
    estimatedtime  daterange NULL,
    userfinishtime date      NULL,
    CONSTRAINT tasks_pk PRIMARY KEY (taskuuid)
);

ALTER TABLE public.tasks
    ADD CONSTRAINT tasks_fk FOREIGN KEY (useruuid) REFERENCES users (useruuid);
ALTER TABLE public.tasks
    ADD CONSTRAINT tasks_fk_1 FOREIGN KEY (skilluuid) REFERENCES skills (skilluuid);


