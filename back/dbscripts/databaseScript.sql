-- DROP SCHEMA public;

CREATE SCHEMA public AUTHORIZATION postgres;

COMMENT ON SCHEMA public IS 'standard public schema';

-- Drop table

-- DROP TABLE public.projects;

CREATE TABLE IF NOT EXISTS public.projects (
	projectuuid uuid NOT NULL,
	useruuid uuid NULL ,
	skilluuid uuid NULL,
	"name" varchar NULL,
	estimatedtime daterange NULL,
	deliverytime daterange NULL,
	duedate date NULL,
	salary money NULL,
	CONSTRAINT projects_pk PRIMARY KEY (projectuuid)
);

-- Drop table

-- DROP TABLE public.skills;

CREATE TABLE IF NOT EXISTS public.skills (
	skilluuid uuid NOT NULL,
	"name" varchar NULL,
	useruuid uuid NULL,
	taskuuid uuid NULL,
	projectuuid uuid NULL,
	skilllogo bytea NULL,
	CONSTRAINT skills_pk PRIMARY KEY (skilluuid)
);

-- Drop table

-- DROP TABLE public.tasks;

CREATE TABLE IF NOT EXISTS public.tasks (
	taskuuid uuid NOT NULL,
	useruuid uuid NULL,
	skilluuid uuid  NULL,
	"name" varchar  NULL,
	difficulty int4 NULL,
	estimatedtime daterange NULL,
	userfinishtime date NULL,
	CONSTRAINT tasks_pk PRIMARY KEY (taskuuid)
);

-- Drop table

-- DROP TABLE public.users;

CREATE TABLE IF NOT EXISTS public.users (
	useruuid uuid NOT NULL,
	skilluuid uuid  NULL,
	taskuuid uuid NULL,
	projectuuid uuid NULL,
	"name" varchar NOT NULL,
	surname varchar NOT NULL,
	dateofbirth date NULL,
	dateofjoining date NULL,
	"role" uuid NULL,
	email varchar NOT NULL,
	"password" varchar NOT NULL,
	picture varchar NULL,
	phonenumber varchar NULL, -- Number of the model.user
	CONSTRAINT users_pk PRIMARY KEY (useruuid)
);

-- Drop table

-- DROP TABLE public.roles

CREATE TABLE IF NOT EXISTS public.roles (
    roleuuid uuid NOT NULL,
    useruuid uuid NOT NULL,
    rolename varchar NULL,
    CONSTRAINT roles_pk PRIMARY KEY (roleuuid)
);
-- Column comments

COMMENT ON COLUMN public.users.phonenumber IS 'Number of the model.user';

ALTER TABLE public.projects ADD CONSTRAINT projects_fk FOREIGN KEY (useruuid) REFERENCES users(useruuid);
ALTER TABLE public.projects ADD CONSTRAINT projects_fk_1 FOREIGN KEY (skilluuid) REFERENCES skills(skilluuid);

ALTER TABLE public.skills ADD CONSTRAINT skills_fk FOREIGN KEY (useruuid) REFERENCES users(useruuid);
ALTER TABLE public.skills ADD CONSTRAINT skills_fk_1 FOREIGN KEY (projectuuid) REFERENCES projects(projectuuid);
ALTER TABLE public.skills ADD CONSTRAINT skills_fk_2 FOREIGN KEY (taskuuid) REFERENCES tasks(taskuuid);

ALTER TABLE public.tasks ADD CONSTRAINT tasks_fk FOREIGN KEY (useruuid) REFERENCES users(useruuid);
ALTER TABLE public.tasks ADD CONSTRAINT tasks_fk_1 FOREIGN KEY (skilluuid) REFERENCES skills(skilluuid);

ALTER TABLE public.users ADD CONSTRAINT users_fk FOREIGN KEY (skilluuid) REFERENCES skills(skilluuid);
ALTER TABLE public.users ADD CONSTRAINT users_fk_1 FOREIGN KEY (projectuuid) REFERENCES projects(projectuuid);
ALTER TABLE public.users ADD CONSTRAINT users_fk_2 FOREIGN KEY (taskuuid) REFERENCES tasks(taskuuid);

ALTER TABLE public.roles ADD CONSTRAINT roles_fk FOREIGN KEY (useruuid) REFERENCES users(useruuid);
