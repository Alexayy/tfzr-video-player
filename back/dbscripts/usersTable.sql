-- Drop table

-- DROP TABLE public.users;

CREATE TABLE IF NOT EXISTS public.users
(
    useruuid      uuid    NOT NULL,
    skilluuid     uuid    NULL,
    taskuuid      uuid    NULL,
    projectuuid   uuid    NULL,
    "name"        varchar NOT NULL,
    surname       varchar NOT NULL,
    dateofbirth   date    NULL,
    dateofjoining date    NULL,
    "role"        uuid    NULL,
    email         varchar NOT NULL,
    "password"    varchar NOT NULL,
    picture       varchar NULL,
    phonenumber   varchar NULL,
    CONSTRAINT users_pk PRIMARY KEY (useruuid)
);

ALTER TABLE public.users
    ADD CONSTRAINT users_fk FOREIGN KEY (skilluuid) REFERENCES skills (skilluuid);
ALTER TABLE public.users
    ADD CONSTRAINT users_fk_1 FOREIGN KEY (projectuuid) REFERENCES projects (projectuuid);
ALTER TABLE public.users
    ADD CONSTRAINT users_fk_2 FOREIGN KEY (taskuuid) REFERENCES tasks (taskuuid);

