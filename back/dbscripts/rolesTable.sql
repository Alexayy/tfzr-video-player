-- Drop table

-- DROP TABLE public.roles

CREATE TABLE IF NOT EXISTS public.roles
(
    roleuuid uuid    NOT NULL,
    useruuid uuid    NULL,
    rolename varchar NULL,
    CONSTRAINT roles_pk PRIMARY KEY (roleuuid)
);

ALTER TABLE public.roles
    ADD CONSTRAINT roles_fk FOREIGN KEY (useruuid) REFERENCES users (useruuid);
