-- Drop table

-- DROP TABLE public.projects;

CREATE TABLE IF NOT EXISTS public.projects
(
    projectuuid   uuid      NOT NULL,
    useruuid      uuid      NULL,
    skilluuid     uuid      NULL,
    "name"        varchar   NULL,
    estimatedtime daterange NULL,
    deliverytime  daterange NULL,
    duedate       date      NULL,
    salary        money     NULL,
    CONSTRAINT projects_pk PRIMARY KEY (projectuuid)
);

ALTER TABLE public.projects
    ADD CONSTRAINT projects_fk FOREIGN KEY (useruuid) REFERENCES users (useruuid);
ALTER TABLE public.projects
    ADD CONSTRAINT projects_fk_1 FOREIGN KEY (skilluuid) REFERENCES skills (skilluuid);


