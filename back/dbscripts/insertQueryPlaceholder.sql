insert into public.users (useruuid, skilluuid, taskuuid, projectuuid, "name", surname, dateofbirth,
						  dateofjoining, "role", email, "password", picture, phonenumber)
	values ('bd38f268-57d7-48cf-8c33-4c9599fb844b', null,
	null, null, 'Aleksa', 'Cakic',
		   '02.06.1996', null, null ,
		   'aleksa.cakic@gmail.com', 'jebemtijdbc', null, '069737988');

insert into public.skills (skilluuid, "name", useruuid, taskuuid, projectuuid, skilllogo)
	values ('371b8347-ec1f-4f20-8e25-31a7d3d1b0e0', 'Java', 'bd38f268-57d7-48cf-8c33-4c9599fb844b',
		   null, null, null);

insert into public.tasks (taskuuid, useruuid, skilluuid, "name", difficulty, estimatedtime, userfinishtime)
	values ('5608fd57-ac8b-4eac-babe-8e7bac0dc404', 'bd38f268-57d7-48cf-8c33-4c9599fb844b',
		   '371b8347-ec1f-4f20-8e25-31a7d3d1b0e0', 'Arrays', 10, null, null);

insert into public.projects (projectuuid, useruuid, skilluuid, "name", estimatedtime, deliverytime,
						duedate, salary)
	values ('5181fa14-b122-4b6c-8040-173d7c1634b5', 'bd38f268-57d7-48cf-8c33-4c9599fb844b', '371b8347-ec1f-4f20-8e25-31a7d3d1b0e0',
		   'Platform', null, null, null, null);

insert into public.roles (roleuuid, useruuid, rolename)
	values ('9c507f14-0098-11ea-8d71-362b9e155667', 'bd38f268-57d7-48cf-8c33-4c9599fb844b', 'Mentor');

/*
Run these scripts to populate the database with place holders. Code is indented and
barely readable due to missing space on the screen, so be careful and read line by line
The SQL dialect is POSGRES, so bare in mind the access to the table,
for you have to access it trough schema access modifier (SCHEMA NAME) which is in this case
called 'public'.
*/