**DCZR Platform**

This model project is internal DevCenter platform for tracking progress of various activities.

Technology Stack
For the MVP stage, we do not want to overcomplicate things with complex and robust frameworks such as is Spring. A simple microframework like Spark will be sufficient. Please find below the desired technology stack for building this platform.

Backend: 
- Java 8
- Spark Java framework
- PostgreSQL
- Docker (used to automate local development)
- Added sql2o framework to easy the querying 

Frontend: 
- TypeScript
- Angular 8
- Bootstrap 4

3rd Party Services: 
- Firebase (Push Notifications)

Tools (to speed up and support development):
- Trello (model.task management and distribution)
- Google Docs (documentation and proposals)
- Slack (team communication)
- Git (version control system - VCS)

Please read everything written in this document and leave comments or start a discussion on a Slack channel. 
For any information, ask Nikola Siker or Nikola Mirosvljev.

**Installing Docker**

***DOCKER ONLY PULLS THE POSTGRES IMAGE, NOT THE DATABASE, 
THATS WHY WE USE SCRIPTS PLEASE LOOK AT THE FOLDER INSIDE THE PROJECT CALLED dbscripts***

*Windows*

Follow the installation instructions on:
    
    https://docs.docker.com/docker-for-windows/install/

*Linux*

    https://docs.docker.com/install/linux/docker-ce/ubuntu/
    
Closely follow instructions for your Linux distribution so that you get the exact repository
and proper dependencies.

**RUNNING THE DOCKER IMAGE**

*RUN LINUX CONTAINERS!!!*

*On Linux, you will have to give permission to super model.user*

Run a command in new container

    docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
    
**example**

    $ docker run --name test -it debian
    root@d6c0fe130dba:/# exit 13
    $ echo $?
    13
    $ docker ps -a | grep test
    d6c0fe130dba        debian:7            "/bin/bash"         26 seconds ago      Exited (13) 17 seconds ago                         test
    
**DEVELOPER NOTES** **IMPORTANT**

When the model.project is pulled, there is a *docker-compose.yaml* that holds the information about the image.
Sql scripts are in the model.project folder, so mind that. 
Once you start docker-compose file, open and import
PSQL scripts into the editor, or run them.
<p>
The command for building the image is:

    $ docker-compose up

This will bring up the docker container with postgres inside. The file contains *USERNAME* and *PASSWORD*
to access the database.

***DOCKER ONLY PULLS THE POSTGRES IMAGE, NOT THE DATABASE, THATS WHY WE USE SCRIPTS***
