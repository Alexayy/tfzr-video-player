import {Component, IterableDiffers, OnInit} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {UserService} from '../user.service';
import {ObjectUnsubscribedError} from 'rxjs';
import * as uuid from 'uuid';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {

  projects: any = [];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.userService.getObjectList().subscribe((data) => {
        console.log(data.body[0].name, data.body[0].projectUUID);
        this.projects = data.body;
        console.log(this.projects[0].projectUUID);
    });
  }

  deleteUser(projectUUID: uuid) {
    // this.userService.deleteObject(this.projectuuid).subscribe(data => {
    //     console.log(data);
    //   },
    //   error => console.log(error));
  }
}
