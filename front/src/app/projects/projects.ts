import { ObjectUnsubscribedError } from 'rxjs';
import * as uuid from 'uuid';

export interface Projects {
    projectuuid: uuid;
    useruuid: uuid;
    skilluuid: uuid;
    name: uuid;
    estimatedtime: any;
    deliverytime: any;
    duedate: Date;
    salary: any;
}
