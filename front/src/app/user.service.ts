import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as uuid from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl = 'http://localhost:4567/allprojects';
  constructor(private http: HttpClient) { }

  getObject(objectUUID: uuid): Observable<any> {
    return this.http.get(this.baseUrl + '/:uuid');
  }

  createObject(object: any): Observable<any> {
    return this.http.post(this.baseUrl, object);
  }

  updateObject(objectUUID: uuid, value: any): Observable<any> {
    return this.http.put((this.baseUrl), value);
  }

  deleteObject(objectUUID: uuid): Observable<any> {
    return this.http.delete(this.baseUrl, { responseType: 'text' });
  }

  getObjectList(): Observable<HttpResponse<any[]>> {
    return this.http.get<any[]>(this.baseUrl, { observe: 'response'});
  }

  getObjects() {
    return this.http.get(this.baseUrl);
  }
}
