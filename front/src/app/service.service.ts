import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(private http: HttpClient) {
    this.getStuff();
  }
  url = 'http://localhost:8080/projects';
  getStuff() {
    console.log('What happens here? Is this invoked?' + this.url);
    return this.http.get(this.url);
  }
}
