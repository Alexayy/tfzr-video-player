import { ObjectUnsubscribedError } from 'rxjs';
import * as uuid from 'uuid';

export interface Roles {
  roleUUID: uuid;
  userUUID: uuid;
  roleName: string;
}
