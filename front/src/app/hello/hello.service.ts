import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HelloService {

  constructor(private http: HttpClient) {
    this.getStuff();
  }
  url = 'http://localhost:8080/projects';
  getStuff() {
    console.log('Service called from hello component ' + this.url);
    return this.http.get(this.url);
  }
}
