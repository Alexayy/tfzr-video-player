import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import {HelloService} from './hello.service';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  aleksa: any;
  constructor(private getReq: HelloService) {
    this.ngOnInit();
  }

  ngOnInit() {
    console.log('Aleksa');
    console.log(this.getReq.getStuff());
    return this.getReq.getStuff().subscribe(this.aleksa);
  }
}
