# DczrFrontEnd

This is the front end part of the project made with Angular 8 and TypeScript. This project uses Angular 8 CLI and Angular's native ```ng``` package manager, but ```npm``` may be used as well. In case of usage of ```npm```, please use it as following:

    npm start

That command will trigger 

    ng serve

with some other dependencies.

This front end project uses following technologies in stack:
- Angular 8 CLI
- Bootstrap 4.3.1 CLI (With its dependencies to run)
  - Popper.js
  - JQuery
- Webpack

**NOTE** *all these technologies, frameworks and libraries will change during the course of the project*

**#developer note**
***dependencies in*** ```angular.json``` ***are resolved, feel free to pull and start working!***

# Good luck, and happy coding!

---------------------

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
