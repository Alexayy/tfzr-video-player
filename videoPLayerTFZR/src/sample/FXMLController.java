package sample;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.binding.Bindings;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import static javafx.scene.media.MediaPlayer.Status.PLAYING;

import javafx.scene.media.MediaView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.swing.JOptionPane;

public class FXMLController implements Initializable {
    public String filePath;
    public MediaPlayer mediaPlayer;

    public FXMLController() {

    }

    public FXMLController(String filePath, MediaPlayer mediaPlayer) {
        this.filePath = filePath;
        this.mediaPlayer = mediaPlayer;
    }

    public FXMLController(String filePath) {
        this.filePath = filePath;
    }

    public FXMLController(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    @FXML
    private Label label;

    @FXML
    private MediaView mediaView;

    @FXML
    private Slider slider;

    @FXML
    private Slider mediaSlider;

    private List<String> extensions;

    @FXML

    private void handleButtonAction(ActionEvent event) {
        extensions = Arrays.asList("*.mp4", "*.3gp", "*.mkv", "*.MP4", "*.MKV", "*.3GP", "*.flv", "*.wmv");
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Select File", extensions);

        try {
            fileChooser.getExtensionFilters().add(filter);
            fileChooser.setTitle("Select Files to Open!");
            File file = fileChooser.showOpenDialog(null);
            filePath = file.toURI().toString();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (filePath != null) {
            Media media = new Media(filePath);
            mediaPlayer = new MediaPlayer(media);
            mediaView.setMediaPlayer(mediaPlayer);
            DoubleProperty width = mediaView.fitWidthProperty();
            DoubleProperty height = mediaView.fitHeightProperty();

            width.bind(Bindings.selectDouble(mediaView.sceneProperty(), "width"));
            height.bind(Bindings.selectDouble(mediaView.sceneProperty(), "height"));

            slider.setValue(mediaPlayer.getVolume() * 100);
            slider.valueProperty().addListener(obeservable -> mediaPlayer.setVolume(slider.getValue() / 100));

            mediaPlayer.setOnReady(() -> {
                mediaSlider.setMin(0.0);
                mediaSlider.setValue(0.0);
                mediaSlider.setMax(mediaPlayer.getTotalDuration().toSeconds());
                mediaPlayer.currentTimeProperty().addListener((observable, oldValue, newValue) -> mediaSlider.setValue(newValue.toSeconds()));
                mediaSlider.setOnMouseClicked(event1 -> mediaPlayer.seek(Duration.seconds(mediaSlider.getValue())));
            });

            mediaPlayer.play();
        }
    }

    @FXML
    private void pauseVideo() {
        try {
            if (mediaPlayer.getStatus() == PLAYING) {
                mediaPlayer.pause();
            } else {
                mediaPlayer.play();
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Open up a File First");
        }
    }

    @FXML
    private void stopVideo() {
        try {
            mediaPlayer.stop();
        } catch (NullPointerException e) {
            //System.out.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "Open up a File First");
        }
    }

    @FXML
    private void fastVideo() {
        try {
            mediaPlayer.setRate(mediaPlayer.getRate() + 0.5);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Open up a File First");
        }
    }

    @FXML
    private void slowVideo() {
        try {
            mediaPlayer.setRate(mediaPlayer.getRate() - 0.5);
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Open up a File First");
        }
    }

    @FXML
    private void about() {
        PopupInherited popupInherited = new PopupInherited();
        popupInherited.display();
    }

    @FXML
    private void closeVideo() {
        System.exit(0);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
}
