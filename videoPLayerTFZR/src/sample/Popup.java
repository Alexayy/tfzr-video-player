package sample;


import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;


public abstract class Popup {

    public Popup() {
        System.out.println("Videce se u terminalu poziv.");
    }

    abstract void display();
}