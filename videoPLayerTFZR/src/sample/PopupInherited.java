package sample;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PopupInherited extends Popup {

    @Override
    public void display() {
        Stage popupwindow = new Stage();
        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("About");

        Label label1 = new Label("Inheritance se nalazi ovde. Postovani," +
                "\n \tmolim Vas da me postedite muke, radim praksu, Game Developer sam, ovako u slobodno vreme sijem " +
                "Javu po ceo dan. Poznajem u skoro sve javine tehnologije. \n" +
                "Najvise radim u SparkJava i springu, mada u poslednje vreme mi se vise svidja MEAN stack." +
                "\n \tZiva je java, i dalje je java majka mikroprocesa. Neuporedivo." +
                "\n \tSrdacan pozdrav," +
                "\n \tAleksa Cakic" +
                "\n \tSI 23/17");
        Button button1 = new Button("Close me :)");
        button1.setOnAction(e -> popupwindow.close());

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label1, button1);
        layout.setAlignment(Pos.CENTER);

        Scene scene1 = new Scene(layout, 300, 250);
        popupwindow.setScene(scene1);
        popupwindow.showAndWait();
    }
}
