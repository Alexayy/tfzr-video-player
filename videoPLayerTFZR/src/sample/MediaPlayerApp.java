package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MediaPlayerApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("Player.fxml"));

        Scene scene = new Scene(root);
        stage.setTitle("VideoPlayer");
        stage.setFullScreenExitHint("Press ESC or Double click to exit the full Screen");

        scene.setOnMouseClicked(clicked -> {
            if(clicked.getClickCount() == 2){
                if(!stage.isFullScreen()){
                    stage.setFullScreen(true);
                } else {
                    stage.setFullScreen(false);
                }
            }
        });

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
